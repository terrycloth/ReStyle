Restyle
=======

A css injector WebExtension.

The configuration is a valid css block of text that is divided into sections by special comment lines.
Sections matching the url of a new page are injected.
