"use strict";

// store contains persisted state.
var store;

// config is the textarea that contains the rules.
var config;

// results contains the outcome of request checks.
var results = [];

try {
    store = chrome.storage.sync;

    var onMessage = chrome.runtime.onMessage;
    if (onMessage.hasListener(handleMessage) !== true) {
        onMessage.addListener(handleMessage);
    }

    var onChanged = chrome.storage.onChanged;
    if (onChanged.hasListener(storeChanged) !== true) {
        onChanged.addListener(storeChanged);
    }
} catch (e) {
    console.error(e);
}

document.addEventListener("DOMContentLoaded", init);

// init registers event handlers and grabs dom element references.
function init() {
    document.getElementById("saveConfig").addEventListener("click", saveConfig);
    document.getElementById("sortResults").addEventListener("click", sortResults);
    document.getElementById("clearResults").addEventListener("click", clearResults);
    document.getElementById("toggleDescription").addEventListener("click", toggleDescription);
    results.e = document.getElementById("results");
    config = document.getElementById("config");
    config.desc = document.getElementById("description");
    config.value = "";

    if (store) {
        store.get(null, initStorage);
    }
}

// initStorage checks is sync storage is available, and falls back to local storage if not.
function initStorage(ok) {
    if (!ok) {
        store = chrome.storage.local;
    }
    storeChanged();
}

// storeChanged handles configuration changes in the store.
function storeChanged() {
    store.get(null, update);
}

function update(conf) {
    if (conf.stylesheet) {
        config.value = conf.stylesheet;
    }
}

function saveConfig() {
    store.set({
        stylesheet: config.value
    });
    clearResults();
}

// toggleDescription shows/hides the description section.
function toggleDescription() {
    config.desc.classList.toggle("hidden");
}

// handleMessage handles extension messages.
function handleMessage(message) {
    if (message.type === "restyle") {
        results.push(message);
        results.e.appendChild(createResultElement(message));
    }
}

// clearResults clears the request history.
function clearResults() {
    results.length = 0;
    clear(results.e);
}

// sortResults sorts the requests and updates the ui.
function sortResults() {
    results = results.sort(compareResults);
    clear(results.e);
    var i, l;
    for (i = 0, l = results.length; i < l; i += 1) {
        results.e.appendChild(createResultElement(results[i]));
    }
}

// compareResults compares the requests by state and url.
function compareResults(a, b) {
    if (a.state !== b.state) {
        return a.state.localeCompare(b.state);
    }
    return a.url.localeCompare(b.url);
}

// createRequestElement returns a dom element for result.
function createResultElement(result) {
    var e = document.createElement("li");
    e.className = "results__result results__result--" + result.state;
    if (result.sections) e.title = result.sections;
    setText(e, result.url);
    return e;
}

// setText replaces the contents of node with a text node containing text.
function setText(node, text) {
    clear(node);
    node.appendChild(document.createTextNode(text));
}

// clear removes all children of node.
function clear(node) {
    while (node.lastChild) node.removeChild(node.lastChild);
}
